FROM fluentd:v1.9.1-debian-1.0

USER root

RUN apt update && \
    apt install -y wget logrotate

RUN wget https://github.com/a2o/snoopy/releases/download/snoopy-2.4.12/snoopy-2.4.12.tar.gz && \
    wget -O install-snoopy.sh https://github.com/a2o/snoopy/raw/install/install/install-snoopy.sh && \
    chmod 755 install-snoopy.sh && \
    ./install-snoopy.sh snoopy-2.4.12.tar.gz

# Copy snoopy config file, so snoopy logs all process outputs to file
COPY snoopy.ini /etc/

# Copy fluentd config file, so fluentd will listen to logs from snoopy
COPY fluent.conf /fluentd/etc/

# Copy snoopy log rotate config, so snoopy log file will be rotated
COPY snoopy.conf /etc/logrotate.d/

# Copy cron job to try rotating snoopy log every interval
COPY snoopy.logrotate /etc/cron.d/

# USER fluent